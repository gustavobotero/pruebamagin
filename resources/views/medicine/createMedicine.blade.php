@extends('layouts.app')
@section('aCreate','active')

@section('content')
    <div class="container">
        <form action="{{Route('medicine.store')}}" method="post">
            @csrf
            @if(session('message'))
                <div class="alert alert-success" id="message">
                    {{session('message')}}
                </div>
            @endif
            <div class="row col-md-8 m-auto">
                <div class="col-md-12 text-center form-group">
                    <h1>CREAR MEDICAMENTO</h1>
                </div>
                <div class="col-md-6 form-group">
                    <label for="ium">Identificador Único de Medicamento:</label>
                <input type="number" class="form-control @errors('ium')" name="ium" id="ium" value="{{old('ium')}}">
                    @if ($errors->has('ium'))
                        <div class="text-danger">
                            {{$errors->first('ium')}}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="medicine">Nombre Medicamento:</label>
                    <input type="text" class="form-control @errors('medicine')" name="medicine" id="medicine" value="{{old('medicine')}}">
                    @if ($errors->has('medicine'))
                        <div class="text-danger">
                            {{$errors->first('medicine')}}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="category">Categoria:</label>
                    <input type="text" class="form-control @errors('category')" name="category" id="category" value="{{old('category')}}">
                    @if ($errors->has('category'))
                        <div class="text-danger">
                            {{$errors->first('category')}}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="content">Contenido:</label>
                    <input type="text" class="form-control @errors('content')" name="content" id="content" value="{{old('content')}}">
                    @if ($errors->has('content'))
                        <div class="text-danger">
                            {{$errors->first('content')}}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="implementation">Implementación:</label>
                    <input type="text" class="form-control @errors('implementation')" name="implementation" id="implementation" value="{{old('implementation')}}">
                    @if ($errors->has('implementation'))
                        <div class="text-danger">
                            {{$errors->first('implementation')}}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="description">Descripción:</label>
                    <input type="text" class="form-control @errors('description')" name="description" id="description" value="{{old('description')}}">
                    @if ($errors->has('description'))
                        <div class="text-danger">
                            {{$errors->first('description')}}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="creation_date">Fecha de creación:</label>
                    <input type="date" class="form-control @errors('creation_date')" name="creation_date" id="creation_date" value="{{old('creation_date')}}">
                    @if ($errors->has('creation_date'))
                        <div class="text-danger">
                            {{$errors->first('creation_date')}}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="expiration_date">Fecha de expiración:</label>
                    <input type="date" class="form-control @errors('expiration_date')" name="expiration_date" id="expiration_date" value="{{old('expiration_date')}}">
                    @if ($errors->has('expiration_date'))
                        <div class="text-danger">
                            {{$errors->first('expiration_date')}}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <button class="btn btn-success btn-lg">Guardar</button>
                    <a href="{{Route('medicine.index')}}" class="btn btn-info btn-lg text-light">Ir a listado</a>
                </div>
            </div>
        </form>
    </div>
@endsection