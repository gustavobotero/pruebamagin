@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-4 m-auto">
        <div class="card-head">
            <h1 class="text-center">Ver medicamento</h1>
        </div>
        <div class="card-body">
            <table class="table">
                <tr>
                    <th>Ium:</th>
                    <td>{{$medicine->ium}}</td>
                </tr>
                <tr>
                    <th>Medicamento:</th>
                    <td>{{$medicine->medicine}}</td>
                </tr>
                <tr>
                    <th>Categoria:</th>
                    <td>{{$medicine->category}}</td>
                </tr>
                <tr>
                    <th>Contenido:</th>
                    <td>{{$medicine->content}}</td>
                </tr>
                <tr>
                    <th>Implementación:</th>
                    <td>{{$medicine->implementation}}</td>
                </tr>
                <tr>
                    <th>Descripción:</th>
                    <td>{{$medicine->description}}</td>
                </tr>
                <tr>
                    <th>Fecha de elaboración:</th>
                    <td>{{$medicine->creation_date}}</td>
                </tr>
                <tr>
                    <th>Fecha de expiración:</th>
                    <td>{{$medicine->expiration_date}}</td>
                </tr>
                                 
            </table>
            <div class="text-center row">
                <div class="col-md-6">
                    <a href="{{Route('medicine.index')}}" class="btn btn-info btn-block text-light">Regresar</a>
                </div>
                <div class="col-md-6">
                    <a href="{{Route('medicine.edit',$medicine->id)}}" class="btn btn-success btn-block text-light">Editar</a>
                </div>
            </div> 
        </div>
    </div>
    
    
   
</div>
    
@endsection