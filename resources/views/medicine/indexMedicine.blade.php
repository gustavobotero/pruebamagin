@extends('layouts.app')
@section('aIndex','active')

@section('content')
    <div class="container">
        <h1 class="text-center">Lista de medicamentos</h1>
        @if(session('message'))
            <div class="alert alert-success" id="message">
                {{session('message')}}
            </div>
        @endif
        <table class="table table-bordered table-striped table-hover" id="myTable" data-route= "{{Route('medicine.showTable')}}">
            <thead>
                <tr class="bg-dark text-light">
                    <th>ACCION</th>
                    <th>#</th>
                    <th>IUM</th>
                    <th>MEDICINA</th>
                    <th>CATEGORIA</th>
                    <th>CONTENIDO</th>
                    <th>IMPLEMENTACION</th>
                    <th>DESCRIPCION</th>
                    <th>FECHA ELABORACION</th>
                    <th>FECHA EXPIRACION</th>
                </tr>
            </thead>
        </table>
    </div>    
@endsection