<?php

namespace App\Repository\Contract;

use App\Models\Medicine;
use App\Http\Requests\MedicineRequest;
use Illuminate\Http\Request;

interface MedicineRepositoryInterface
{
    public function create(MedicineRequest $medicineData): Medicine;

    public function getId(int $medicineData): Medicine;

    public function update(Request $medicineData, int $id): Medicine;

    public function delete(int $id): Medicine;
}
