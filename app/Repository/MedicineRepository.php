<?php

namespace App\Repository;

use App\Http\Requests\MedicineRequest;
use App\Models\Medicine;
use App\Repository\Contract\MedicineRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MedicineRepository implements MedicineRepositoryInterface
{


    public function create(MedicineRequest $medicineData): Medicine
    {
        return Medicine::create($medicineData->all() + ['user_id' => Auth::user()->id]);
    }

    public function getId(int $medicineData): Medicine
    {
        return Medicine::find($medicineData);
    }

    public function update(Request $medicineData, int $id): Medicine
    {
        $data = $this->getId($id);
        $data->update($medicineData->all());

        return $data;
    }

    public function delete(int $id): Medicine
    {
        $medicine = $this->getId($id);
        $medicine->delete();

        return $medicine;
    }
}
