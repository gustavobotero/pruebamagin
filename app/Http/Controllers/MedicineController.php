<?php

namespace App\Http\Controllers;

use App\Models\Medicine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\MedicineRequest;
use App\Repository\Contract\MedicineRepositoryInterface;
use App\Usecases\Contracts\Medicine\ValidaFechaVencimientoInterface;
use Illuminate\Support\Facades\DB;

class MedicineController extends Controller
{
    protected $medicineRepository;

    protected $validaFechaVencimiento;

    public function __construct(
        MedicineRepositoryInterface $medicineRepository,
        ValidaFechaVencimientoInterface $validaFechaVencimiento
    ) {
        $this->medicineRepository = $medicineRepository;
        $this->validaFechaVencimiento = $validaFechaVencimiento;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('medicine.indexMedicine');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medicine.createMedicine');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicineRequest $request)
    {
        DB::beginTransaction();

        try {

            if (!$this->validaFechaVencimiento->handle($request->expiration_date)) {

                return redirect()->back()->with(['message' => 'Error validacion fecha de vencimiento']);
            }

            $this->medicineRepository->create($request);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with(['message' => $e->getMessage()]);
        }


        return redirect()->route('medicine.index')->with(['message' => 'Medicamento creado correctamente']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $medicine = $this->medicineRepository->getId($id);

        return view('medicine.showMedicine', compact('medicine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medicine = $this->medicineRepository->getId($id);

        return view('medicine.editMedicine', compact('medicine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {

            $this->medicineRepository->update($request, $id);

            DB::commit();
        } catch (\Exception $e) {

            DB::rollback();
        }


        return redirect()->route('medicine.index')->with(['message' => 'Medicamento actualizado correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {

            $medicine = $this->medicineRepository->delete($id);

            DB::commit();
        } catch (\Exception $e) {

            DB::rollback();
        }


        if ($medicine) {
            $res = ['status' => true, 'id' => $id];
        } else {
            $res = ['status' => false, 'message' => 'No se pudo eliminar porque el registro está asociado con otros registros.'];
        }
        return $res;
    }

    public function showTable()
    {
        return datatables(Medicine::where('user_id', Auth::user()->id))->toJson();
    }
}
