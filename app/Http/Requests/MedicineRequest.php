<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ium' => 'required|min:2|unique:medicines|numeric',
            'medicine' => 'required|min:2',
            'category' => 'required|min:2|string',
            'content' => 'required|min:2',
            'implementation' => 'required|min:2',
            'description' => 'required|min:2',
            'creation_date' => 'required|date',
            'expiration_date' => 'required|date'
        ];
    }
}
