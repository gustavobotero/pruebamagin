<?php

namespace App\Usecases\Contracts\Medicine;

interface ValidaFechaVencimientoInterface
{
    public function handle(string $fecha): bool;
}