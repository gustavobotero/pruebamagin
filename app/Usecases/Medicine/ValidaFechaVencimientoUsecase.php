<?php

namespace App\Usecases\Medicine;

use App\Usecases\Contracts\Medicine\ValidaFechaVencimientoInterface;

/**
 * Undocumented class
 */
class ValidaFechaVencimientoUsecase implements ValidaFechaVencimientoInterface
{
    /**
     * Undocumented function
     *
     * @param string $fecha
     * @return boolean
     */
    public function handle(string $fecha): bool
    {
        return false;
    }
}
