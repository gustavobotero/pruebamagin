<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    protected $table = 'medicines';

    protected $keyType = 'integer';

    protected $fillable = [
        'user_id',
        'ium',
        'medicine',
        'category',
        'content',
        'implementation',
        'description',
        'creation_date',
        'expiration_date'
    ];

    protected $hidden = [
        'created_at',
        'deleted_at'
    ];
}
