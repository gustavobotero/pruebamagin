$(function(){
  $('#myTable').DataTable({
		    processing: true,
        serverSide: true,
        aaSorting: [],
        language:{
        		url: window.laravel.url+"/js/data-table-spanish.json",
        		searchPlaceholder: "Buscar..."
    	},
        ajax: {
            url: $('#myTable').data('route'),
            type: "POST",
            data: {"_token":window.laravel.token}
        },
        columns: [
            { data: 'id', name: 'medicines.id' },
            { data: 'id', name: 'medicines.id' },
            { data: 'ium', name: 'medicines.ium' },
            { data: 'medicine', name: 'medicines.medicine' },
            { data: 'category', name: 'medicines.category' },
            { data: 'content', name: 'medicines.content' },
            { data: 'implementation', name: 'medicines.implementation' },
            { data: 'description', name: 'medicines.description' },
            { data: 'creation_date', name: 'medicines.creation_date' },
            { data: 'expiration_date', name: 'medicines.expiration_date' },
        ],
        columnDefs: [
		    {
		        targets: 0,
		        orderable:false,
		        createdCell: function (td, cellData, rowData, row, col){
		        	//Mostrar botones con los id de cada registro
              var html =  '<a href="'+window.laravel.url+'/medicina/'+cellData+'" class="btn btn-info      btn-block text-light">Ver</a>'
                          +'<a href="'+window.laravel.url+'/medicina/'+cellData+'/edit" class="btn btn-success      btn-block">Editar</a>'
					                +'<button class="remove btn btn-danger btn-block" onclick="deleteRow('+cellData+')">Eliminar</button>';
					$(td).html(html);
		        }
		    },
		    {
		        targets: 1,
		        orderable:false,
		        createdCell: function (td, cellData, rowData, row, col) {
		            $(td).html(++row);
		        }
		    }
	    ]
	});
	
});

function deleteRow(id){
  bootbox.confirm({
    message: "¿Está seguro de eliminar el registro?",
    buttons: {
        confirm: {
            label: 'Si',
            className: 'btn-danger'
        },
        cancel: {
            label: 'No',
            className: 'btn-primary'
        }
    },
  callback: function (result) {
    if(result){
      $.ajax({
        url:window.laravel.url+'/medicina/'+id,
				type: 'POST',
				data: {_token:window.laravel.token,
						   _method:'DELETE'},
      })
      .done(function(res) {
        if(res.status){
          $('#myTable').DataTable().ajax.reload();
					}else{
						alert(res.message);
          }
        }).fail(function(res) {
            console.log(res);
            console.log("error:");
            
          });
	  }
	}
	});	
}


