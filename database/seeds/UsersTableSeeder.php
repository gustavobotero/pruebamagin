<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'num_document' => '1110520398',
                'name' => 'Gustavo Botero',
                'email' => 'gustavo@botero.com',
                'password' => bcrypt('secret')
            ],
            [
                'num_document' => '123456789',
                'name' => 'Otro usuario',
                'email' => 'otro@usuario.com',
                'password' => bcrypt('secret')
            ]
        ]);
    }
}
