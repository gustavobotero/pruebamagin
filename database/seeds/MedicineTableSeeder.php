<?php

use Illuminate\Database\Seeder;

class MedicineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('json/medicine.json');
        $json = file_get_contents($file);

        foreach(json_decode($json) as $item){
            DB::table('medicines')->insert([
                'user_id' => $item->user_id,
                'ium' => $item->ium,
                'medicine' => $item->medicine,
                'category' => $item->category,
                'content' => $item->content,
                'implementation' => $item->implementation,
                'description' => $item->description,
                'creation_date' => $item->creation_date,
                'expiration_date' => $item->expiration_date
            ]);
        }
    }
}
