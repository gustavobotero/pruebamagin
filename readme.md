Elaborado por: Gustavo Adolfo Botero Villalobos

Datos de ingreso al sistema:
1. documento = 1110520398
contraseña = secret

2. documento = 123456789
contraseña = secret

El proyecto fue creado con las siguientes tecnologías:
1. Laravel 5.8
2. Bootstrap
3. jQuery
4. MySQL

Se usaron unas librerías:
1. DataTable
2. Bootbox
3. DataTablet – Yajra

La persona se va a poder registrar, después de registrado va poder crear, editar, ver y eliminar los registros, solo podrá ver los registros que el cree.

La consulta a la base de datos para listar todos los productos se hizo mediante ajax, lo mismo para la eliminación de un producto.


